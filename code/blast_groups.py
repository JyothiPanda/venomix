#!/usr/bin/python
"""
This file retrieves information from the BLAST output and the 
ToxProt information file ().
The script first takes the UniProt table (uniref_50), but you can upload
your own tabular dataset relative to the query sequences.
together by linking "funcitonal groups" with Transcripts.
Version 1.0 (Jason Macrander)
"""
import re
from Function import Convert_Blast

Blast_Out = open("../data/Blast_out_E-3", 'r')
Blast_Groups = open ("../data/ToxProt/uniref_50.txt", 'r')
TPM_List = open("../data/Intermediate/Mg-T_1.fasta")

HitD = {}
QueryD = {}

QueryD,HitD = Convert_Blast (Blast_Out)


QueryKeyList = QueryD.keys()
#QueryID = QueryKeyList.split("|")

Groups = {}
#print QueryKeyList
LineCount = 0
Gene_group = [] 
for line in Blast_Groups:
	LineCount = LineCount + 1
	keyCount = 0
	keyCount1 = 0
	if LineCount == 1:
		next
	if LineCount >= 2:
		U50 = line.split("\t")
		ACCESSION = U50[4].split(" ;")
		for ID in ACCESSION:
			ACC = ID.split("; ")
		GeneC = U50[2].split(": ")
		Gene_group = ''.join(GeneC[1:])
		Gene_group = Gene_group.replace(" (Fragments)", "")
		Gene_group = Gene_group.replace(" (Fragment)", "")
		numbers = U50[3] #This is the problem, when put into Gene_groups it stays the same value in the print GeneGroups, but when you list keys at the end all the values change to 1
		Gene_key = "%s(%s)" % (Gene_group,numbers)
		#print numbers
		if numbers > 1:
			keyCount == keyCount + 1

		#print "gene key (ln 44):", Gene_key
		#try:
			#print "->{}<-numbers".format(numbers)
			#print "IF :", GeneGroups
		#except:
			#print "ERROR:", U50[3]
		#Gene_groups = "%s(%d)" % (Gene_group, numbers) 
#		print Gene_groups
		#print Gene_groups
		#if numbers >
		for Akey in ACC:
		#	print ID
			if Akey in QueryKeyList:
				#print "Akey", Akey
				keyCount1 = keyCount1 + 1
				Groups[Gene_key] = Groups.get(Gene_key,[]) + list(set(QueryD[Akey]))
				#print keyCount1

print Groups.keys()
print "keys total:" , len(Groups.keys())
#				print "FOR :" , Gene_key
#				
#
#print Groups.keys()
#print len(Groups.keys())
#print Groups
#for elements in Groups[Gene_group]:
#	print Groups[elements]