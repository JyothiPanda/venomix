#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Extract sequences from a fasta file if their name is in a 'wanted' file.
Wanted file contains one sequence name per line.
Usage:
    %program <input_file> <wanted_file> <output_file>

    Written by enomandeau (https://github.com/enormandeau)"""

import sys
import re

Blast_Out = open("../data/Blast_out_E-3", 'r')
    for line in Blast_Out:
        split_line = line.split('\t') #split tabular blast output
        hit = split_line[2] #retrieve query information from line
 
        #focus = query.split("|") #Isolate UniProt ID for downstream matching
        #focus = focus [1]
        #QueryD[focus] = QueryD.get(focus,[]) + [hit]
        #HitD[hit] = HitD.get(hit,[]) + [focus]

    return (QueryD,HitD)




try:
    from Bio import SeqIO
except:
    print "This program requires the Biopython library"
    sys.exit(0)

try:
    fasta_file = sys.argv[1]  # Input fasta file
    wanted_file = sys.argv[2] # Input wanted file, one gene name per line
    result_file = sys.argv[3] # Output fasta file
except:
    print __doc__
    sys.exit(0)

wanted = set()
with open(wanted_file) as f:
    for line in f:
        line = line.strip()
        if line != "":
            wanted.add(line)

fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')

with open(result_file, "w") as f:
    for seq in fasta_sequences:
        name = seq.id
        if name in wanted and len(seq.seq) > 0:
            wanted.remove(name) # Output only the first appearance for a name
            SeqIO.write([seq], f, "fasta")