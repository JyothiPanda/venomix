#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

# {{{ Imports
import os
import datetime
from urllib.request import urlopen
from urllib.parse import urlencode, quote
# }}}


# {{{ get_date_modified()
def get_date_modified():

    """ {{{ Docstrings

    Gets the date the local UniProt reference table file was last modified.

    }}} """

    # get the current working directory (root of Venomix file hierarchy?)
    cwd = os.getcwd()
    # get the directory data is stored in
    data_dir = os.path.join(cwd, 'data')
    # get files stored in data directory
    files_in_dir = os.listdir(data_dir)
    # check if toxprot data file exists in data directory (should be only one
    # file in data directory which matches this creiteria)
    toxprot_dataf = [f for f in files_in_dir if 'toxprot' in f]
    # if toxprot data file exists, get date and time of last modification of
    # current data
    # if toxprot_dataf:
    try:
        # format last modification date time should be written in
        # see https://docs.python.org/3.5/library/datetime.html#strftime-strptime-behavior
        # for explanation
        dt_frmt = '%d_%m_%Y'
        # modify filename to reflect format
        last_mod = (toxprot_dataf[0]).split('.')[0]
        # set last_mod to minimum datetime
        last_mod = datetime.datetime.strptime(last_mod, dt_frmt)
    # will return value error if file name not congruent with format
    except(ValueError, IndexError):
        message = (
                'ToxProt data file written in expected format not found '
                'in directory: {0}'
                ).format(data_dir)
        print(message)
        last_mod = datetime.datetime.min
    return last_mod
# }}}


# {{{ toxprot_url()
def toxprot_url():

    """ {{{ Docstrings

    Returns full URL referencing the animal toxin annotation project database;
    (www.uniprot.org/program/Toxins).

    }}} """

    base_url = 'http://www.uniprot.org/uniref/'
    query = (
            'uniprot:(taxonomy:\"Metazoa [33208]\" (keyword:toxin OR '
            'annotation:(type:\"tissue specificity\" venom)) AND '
            'reviewed:yes) identity:0.5'
            )
    url_values = {
            'format': 'tab',
            'columns': 'id,name,members,sequence,published'
            }
    full_url = '{0}?query={1}&{2}'.format(
            base_url, quote(query, safe='/,:,(,),[,]'),
            urlencode(url_values, safe='/,,')
            )
    return full_url
# }}}


# {{{ get_toxprot_table()
def get_toxprot_table():

    """ {{{ Docstrings

    Downloads the latest ToxProt data if local data file not up to date.

    }}} """

    full_url = toxprot_url()
    toxprot_req = urlopen(full_url)
    remote_lmod = toxprot_req.getheader('Last-Modified')
    frmt = '%a, %d %b %Y %X %Z'
    remote_lmod = datetime.datetime.strptime(remote_lmod, frmt)
    local_lmod = get_date_modified()
    if local_lmod < remote_lmod:
        print('Downloading new version of ToxProt dataset.')
        toxprot_page = toxprot_req.read()
        # get the current working directory (root of Venomix file hierarchy?)
        cwd = os.getcwd()
        # get the directory data is stored in
        toxprot_dataf = os.path.join(
                cwd, 'data', '{0}.toxprot.txt'.format(
                    datetime.datetime.strftime(remote_lmod, '%d_%m_%Y')
                    )
                )
        with open(toxprot_dataf, 'wb') as toxprot_file:
            toxprot_file.write(toxprot_page)
    else:
        print('Already up to date.')
# }}}
