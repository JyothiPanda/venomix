#!/usr/bin/python
"""
Programing Suite: Venomix(xxx) - Functions
This file containes different functions. These functions are assocated
with different steps found in other scripts:
	Seq_Extract.py
	Blast_groups.py
	Blast_report_FULL.py
	Blast_report_SUM.py

Version 0.7 (Jason Macrander)
"""

#Import Functions
import sys
import re
import numpy
from Bio import SeqIO
from Bio import GenBank
from Bio.SeqIO.FastaIO import SimpleFastaParser


"""
External files used in the functions below:
"""
Blast_Groups = open ("../data/ToxProt/uniref_50.txt", 'r')
TPM_List = open("../data/Intermediate/Mg-T_1.fasta")

"""
Write to files needed to poen for used in the functions below:
"""
GenBankSummary = open("../data/out/GB_Summary.md", 'w')
GenbankFull = open("../data/out/GB_Full.md", 'w')

"""
loop through each line of the RSEMFile, determine if it is a 
.genes or .isoforms file, put the items from that file into a dictionary.
"""
def RSEMtoDict(RSEMFile):
	Info_dic = {}
	LineCount = 0
	for Line in RSEMFile:
		LineCount += 1
		if LineCount == 1:
			next
		if LineCount >= 2:	
			Line = Line.strip('\n')
			#If the file looks like a .genes output.
			
			ElementList = (Line.split('\t'))
			#print ElementList
			RSEM_dic = {"ID": ElementList[1], 
			"Length": ElementList[2], 
			"E_length" : ElementList[3], 
			"E_count" : ElementList[4], 
			"TPM" : ElementList[5], 
			"FKPM" : ElementList[6]}

			if Line.count('\t') == 6:
				RSEM_dic["IsoPct"] = None
				RSEM_dic["Type"] = "gene"
			elif Line.count('\t') == 7:
				RSEM_dic["IsoPct"] = ElementList[7]
				RSEM_dic["Type"] = "isoform"
			else:
				print "Unknown file type."
				return
			Info_dic[ElementList[0]] = RSEM_dic
	#print Info_dic
	return Info_dic
"""
Comment about function
"""
def Combine_Files (RSEM,SeqFIle):
	Seq_ID = []
	Sequence = []
	with SeqFIle as handle:
		for values in SimpleFastaParser(handle):
			Seq_ID = values [0]
			#print Seq_ID
			Sequence = values [1]
			#print Sequence
			Fasta_ID = Seq_ID.partition(" ")[0]
			if Fasta_ID == 'comp1000001_c0_seq1': #something in this sequence screwing everything up.
				next
			else:
				RSEM[Fasta_ID]["Sequence"] = Sequence
	return RSEM	
"""
Function is still being worked on
"""
def useGet(KeyList,SearchDict):
	'''Use .get(), with a default value'''
	Added = 0
	for TempKey in KeyList:
		Added = Added + SearchDict.get(TempKey,0)
	return Added

"""
Comment about function
"""

def Convert_Blast(Blast_out_name):
	Blast_Out = open(Blast_out_name, 'r')
	HitD = {}
	QueryD = {}
	for line in Blast_Out:
		split_line = line.split('\t') #split tabular blast output
		query, hit = split_line[:2] #retrieve query information from line
		focus = query.split("|") #Isolate UniProt ID for downstream matching
		focus = focus [1]
		QueryD[focus] = QueryD.get(focus,[]) + [hit]
		HitD[hit] = HitD.get(hit,[]) + [focus]

	return (QueryD,HitD)


"""
This file retrieves information from the BLAST output and the 
ToxProt information file (). The script first takes the UniProt 
table (uniref_50), but you can upload your own tabular dataset 
relative to the query sequences. together by linking 
"funcitonal groups" with Transcripts.
"""

def Tox_Groups (Tox_Cluster, QueryD, Blast_out_name, gbkFile):
	gbk = open(gbkFile, 'r')
	Blast_Out = open(Blast_out_name, 'r')
	Tox_File = open (Tox_Cluster, 'r')
	QueryKeyList = QueryD.keys()
	#print QueryKeyList
	Groups = {}
	Genbank_Summary = {}
	Genbank_Full ={}
	#print QueryKeyList
	Full_out = {}
	#puts the Genbank_Summary Dictionary together
	for record in SeqIO.parse(gbk, "genbank"):
		temp_ID = record.id.split(".")
		gb_ID = temp_ID[0]
#		for thing in dir(record): #prints .record options from Genbank
#			print thing
		record_string = record
		Genbank_Full[gb_ID] = record_string
		temp_Desc = (record.description)
		temp_Desc = gb_ID + ": " + temp_Desc[14:]
		temp_Desc = temp_Desc.split(";")
		temp_Desc = temp_Desc[0]
		Description = [' '.join(temp_Desc.split())]
		#print Description[0]
		Genbank_Summary[gb_ID] = Genbank_Summary.get(gb_ID, []) + list(set(Description)) 
											#Groups.get(Gene_key,[]) + list(set(QueryD[Akey]))
	#puts the Genbank_Full Dictionary together
#	for record in SeqIO.parse(gbk, "genbank"):
#		print record.id
	#Begin matching ToxClusters with BLAST outputs
	LineCount = 0
	Gene_group = [] #<-Probably not needed?
	GBS_FINAL = {}
	GBS_SUMMARY ={}
	for line in Tox_File:
		#print line
		LineCount = LineCount + 1
		keyCount = 0
		keyCount1 = 0
		if LineCount == 1:
			print "Skipping Header"
		if LineCount >= 2:
			#print line
			U50 = line.split("\t")
			ACCESSION = U50[4].split("; ")
			for ID in ACCESSION:
				ACC = ID.split("; ")
			GeneC = U50[2].split(": ")
			Gene_group = ''.join(GeneC[1:])
			Gene_group = Gene_group.replace(" (Fragments)", "")
			Gene_group = Gene_group.replace(" (Fragment)", "")
			numbers = U50[3] #This is the problem, when put into Gene_groups it stays the same value in the print GeneGroups, but when you list keys at the end all the values change to 1
			Gene_key = "%s(%s)" % (Gene_group,numbers)
			#print numbers
			if numbers > 1:
				keyCount == keyCount + 1
			for Akey in ACC:
				if Akey in QueryKeyList:
					#print "Akey", Akey
					keyCount1 = keyCount1 + 1
					Groups[Gene_key] = Groups.get(Gene_key,[]) + list(set(QueryD[Akey]))
					#print Gene_key
					#print keyCount1
			
					GBS_FINAL[Gene_key] = Genbank_Summary.get(Akey,[]) + list(set(Genbank_Summary[Akey]))

			#		HitD[hit] = HitD.get(hit,[]) + [focus]
#
			#for item in Groups:
			#	for Akey in ACC:
			#		if Akey in QueryKeyList:
			#			
			#			 
			#for Akey in Genbank_Summary:
			#	next
				#GB_Final[]

					#print Genbank_Summary[Akey]

			#	for Akey in Genbank_Full:
			#		Full_out[Gene_key] = Genbank_Full[Akey] 
	#print GBS_FINAL.keys()
	#for key in Genbank_Summary:
	#	print "%s:\t%s" % (key, Genbank_Summary[key])
	#for Gene_key in GBS_FINAL:
	#	print "%s:%s" % (Gene_key, GBS_FINAL[Gene_key])
	#for Gene_key in GBS_FINAL:
	#	print GBS_Final #"%s : %s"(GBS_FINAL[Gene_key], Genbank_Summary[gb_ID])
		#print item[Genbank_Full]
	print "Printing to Summary File"
	#print Genbank_Summary
	print "Printing to Full File... this may take a bit"

#
#								Fn = (len(record.features))
#								Features.append(Fn)
#								for record id 
#									if record
#							#print max(Features)
#							#print min(Features)
#							#print sum(Features)/float(len(Features))
#							for record in SeqIO.parse(gbk, "genbank"):
#								Fn = (len(record.features))
#								Features.append(Fn)
#									print record

	#print Groups.keys()
	print "Toxin gene groups: " , len(Groups.keys())
	#print "Containing %s of transcripts: " % ()
	Blast_Out.close()
	Tox_File.close()	
	return (Groups, Genbank_Summary, Genbank_Full)

"""
This function retrieves information from the ToxProt.txt file 
and writes to a dictionary, grouping the file based on the 
left line indicators.
"""

def Genbank_Groups(gbkFile, Tox_Cluster):

	Tox = open(Tox_Cluster, 'r')	
	return (Groups_GBINFO)

def Genbank_Summary(Blast_out_name, gbkFile, Tox_Cluster):
	gbk = open(gbkFile, 'r')
	Blast = open(Blast_out_name, 'r') # might not need this
	LineCount = 0
	Gene_group = [] 
	Tox=open(Tox_Cluster, 'r')
	a=[]
	for line in Tox:
		LineCount = LineCount + 1
		if LineCount == 1:
			print "Skipping Headers"
		if LineCount >= 2:
			BlastLine = line.split("\t")
			query =	BlastLine[0]
			#focus = query.split("|") #Isolate UniProt ID for downstream matching
			focus = query [1]

			for line in Tox:
				#print line
				LineCount = LineCount + 1
				keyCount = 0
				keyCount1 = 0
				if LineCount == 1:
					print "Skipping Header"
				if LineCount >= 2:
					#print line

					Fn=()
					Features = []	##prints summary for Genbank entries
					for record in SeqIO.parse(gbk, "genbank"):
						Fn = (len(record.features))
						Features.append(Fn)
						#for record id 
							#if record
						#print max(Features)
						#print min(Features)
						#print sum(Features)/float(len(Features))
						for record in SeqIO.parse(gbk, "genbank"):
							Fn = (len(record.features))
							Features.append(Fn)
							#print record
							a.append(record)

	
	#	print "Record %s has %i features" % (record.name,len(record.features))
	#	print record.id
	#	print record.name
	#	print record.description
	#	print record.comments

	#	print repr(record.seq)
	print "COMPLETED"

	#gbkFile.close()
	return a

def Genbank_Full(Blast_out_name):
	Blast_Out = open(Blast_out_name, 'r')
	HitD = {}
	QueryD = {}
	for line in Blast_Out:
		split_line = line.split('\t') #split tabular blast output
		query, hit = split_line[:2] #retrieve query information from line
		focus = query.split("|") #Isolate UniProt ID for downstream matching
		focus = focus [1]
		QueryD[focus] = QueryD.get(focus,[]) + [hit]
		HitD[hit] = HitD.get(hit,[]) + [focus]



"""
This function retrieves information from the Tox_Groups output and the 
to categorize candidate toxin genes into a directory (date specific).
Fasta files for each cluster of toxin candidates are labeled according 
to their associative toxin groups.
"""

#def Divide_Seqs ()
